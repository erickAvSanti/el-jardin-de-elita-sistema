<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware'=>['auth','role:admin']],function(){
	Route::prefix('usuarios')->group(function () {
		Route::get('padres/list', 'UsuariosPadresController@list');
	    Route::resource('padres', 'UsuariosPadresController',['as'=>'usuarios']);
	});

	Route::get('usuarios/list', 'UsuariosController@list');
	Route::resource('usuarios', 'UsuariosController');
	Route::resource('roles', 'RolesController');
	Route::resource('permissions', 'PermissionsController');
});

Route::get('/home', 'HomeController@index')->name('home');
 

//Route::resource('padres', 'UsuariosPadresController',['as'=>'usuarios']);