<?php

use Illuminate\Database\Seeder;

use App\Permission;
use App\Role;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();  
        $permission = Permission::get();
        $role_admin = Role::where("name","admin")->first();
        $role_father = Role::where("name","father")->first();
        $role_auxiliar = Role::where("name","auxiliar")->first();
        $role_miss = Role::where("name","miss")->first();
        foreach ($permission as $key => $value) {
            $role_admin->attachPermission($value);
        } 
        $user = [
                'name' => 'Admin User', 
                'email' => 'ecavaloss@gmail.com', 
                'password' => Hash::make('ZapatitodelPeru'),
            ];
        $user = User::create($user); 
        $user->attachRole($role_admin);
        factory(App\User::class,50)->create()->each(
        	function($user) use ($role_father){
        		$user->attachRole($role_father);
        	}
        );
        factory(App\User::class,3)->create()->each(
        	function($user) use ($role_auxiliar){
        		$user->attachRole($role_auxiliar);
        	}
        );
        factory(App\User::class,5)->create()->each(
        	function($user) use ($role_miss){
        		$user->attachRole($role_miss);
        	}
        );
    }
}
