<?php

use Illuminate\Database\Seeder;

use App\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('permissions')->delete();
        $permissions = [
            [
                'name' => 'role-create',
                'display_name' => 'Create Role',
                'description' => 'Create New Role'
            ],
            [
                'name' => 'role-list',
                'display_name' => 'Display Role Listing',
                'description' => 'List All Roles'
            ],
            [
                'name' => 'role-update',
                'display_name' => 'Update Role',
                'description' => 'Update Role Information'
            ],
            [
                'name' => 'role-delete',
                'display_name' => 'Delete Role',
                'description' => 'Delete Role'
            ],
        	[
        		"name"=>"user-create",
        		"display_name"=>"Create user",
        		"description"=>"Create new user",
        	],
        	[
        		"name"=>"user-update",
        		"display_name"=>"Update user",
        		"description"=>"Update specific user",
        	],
        	[
        		"name"=>"user-delete",
        		"display_name"=>"Delete user",
        		"description"=>"Delete a specific user",
        	],
        	[
        		"name"=>"user-list",
        		"display_name"=>"List user",
        		"description"=>"List all user",
        	],
        ];
        foreach ($permissions as $key => &$value) {
        	Permission::create($value);
        }
    }
}
