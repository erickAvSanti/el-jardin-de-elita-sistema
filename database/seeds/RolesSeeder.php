<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('roles')->delete();
        $roles = [
        	[
        		'name'=>'admin',
        		'display_name'=>'System admin',
        		'description'=>'System Administrator',
        	],
        	[
        		'name'=>'father',
        		'display_name'=>'User Father',
        		'description'=>'School user father',
        	],
        	[
        		'name'=>'miss',
        		'display_name'=>'User Miss',
        		'description'=>'School user miss',
        	],
        	[
        		'name'=>'auxiliar',
        		'display_name'=>'User auxiliar',
        		'description'=>'School user auxiliar',
        	],
        ];
        foreach ($roles as $key => &$value) {
        	Role::create($value);
        }
    }
}
