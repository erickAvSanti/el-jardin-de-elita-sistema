
<?php echo view("header");?>
<?php echo view("left_bar");?>

<div class="content-wrapper"> 
  <section class="content-header">
    <h1>
      Dashboard
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Usuarios</li>
    </ol>
  </section> 
  <section class="content">  

    <div id="usuarios-table"> 
      <div class="form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
        </div>
      </div>
      <usuarios-table-component></usuarios-table-component>
    </div> 
    <script src="<?php echo env('APP_URL');?>/js/usuarios/usuarios-table.js"></script>
  </section> 
</div>  
<?php echo view("right_bar");?>
<?php echo view("footer");?> 